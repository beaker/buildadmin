<?php
return [
    'remark_text' => 'When the same file is uploaded many times, only one file will be saved to disk and add an attachment record. Deleting and editing the data in the attachment record will not affect the file itself!',
];